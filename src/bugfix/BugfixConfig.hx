package bugfix;

import etwin.Error;
import etwin.Obfu;
import patchman.module.Data;

@:build(patchman.Build.di())
class BugfixConfig {
  /**
  Prevent players from getting stuck on top of a lateral portal field.
  **/
  public var canBeStuckOnTopOfLateralVortices(default, null): Bool = true;

  /**
  Prevents music from restarting if the same music (same id) is played twice in a row.
  **/
  public var startAgainMusicIfSameID(default, null): Bool = true;

  /**
  Fix the `lifesharing` option.
  **/
  public var lifeSharingInMulti(default, null): Bool = true;

  /**
  Prevent darkness from being applied multiplied times in multiplayer mode.
  **/
  public var reducedDarknessInMulti(default, null): Bool = true;

  /**
  Fix application of multiple extra lives from score steps.
  **/
  public var consecutiveScoreLevels(default, null): Bool = true;

  /**
  Prevent lychees from breaking the order.
  **/
  public var lycheesBreakOrder(default, null): Bool = true;

  /**
  Reduce lag in long runs.
  **/
  public var progressiveLagInLongRuns(default, null): Bool = true;

  /**
   Ensure strawberries can't miss when throwing their ball.
   */
  public var strawberriesCanMissEachOther(default, null): Bool = true;

  /**
  Remove invisible portals left by `openPortal` after exiting a level.  
  
  WARNING: This fix won't work if Merlin < v0.14.2 is used.
  **/
  public var invisiblePortalsWhenReenteringLevel(default, null): Bool = true;

  /**
  Fix player getting stuck in certain animations when changing disguises while controls are locked.
  **/
  public var loopingAnimationsWhenChangingDisguisesCanSoftlock(default, null): Bool = true;

  /**
  Fix certain animations looping indefinitely when changing disguises.
  **/
  public var loopingAnimationsWhenChangingDisguises(default, null): Bool = true;

  /**
  Prevents the player from being hit by explosions and projectiles while warping.
  **/
  public var intangibleWhileWarpingEIBombs(default, null): Bool = true;
  public var intangibleWhileWarpingRedBombs(default, null): Bool = true;
  public var intangibleWhileWarpingHits(default, null): Bool = true;

  /**
  Fix player activating triggers and fields at the previous level's position after a warp.
  **/
  public var playerPositionAfterWarp(default, null): Bool = true;

  /**
  Prevents the Pocket Guu rain from being disabled when not enabling the graphical details.
  **/
  public var keepPocketGuuRain(default, null): Bool = true;

  /**
  Fix off-by-one indicators in map screen when entering a land.
  **/
  public var mapScreenIndicators(default, null): Bool = true;

  /**
  Fix minor graphical glitches when displaying negative scores.
  **/
  public var negativeScoreDisplay(default, null): Bool = true;

  /**
  Fix fields at the player spawn.
  **/
  public var raysAtRespawn(default, null): Bool = false;

  /**
  Fix fields not working in the top row (`y == 0`)
  **/
  public var raysOnHighestLine(default, null): Bool = false;

  /**
  Fix teleport fields not working in the top row (`y == 0`)
  **/
  public var teleportersOnHighestLine(default, null): Bool = false;

  /**
  Fix fruits stuck in a 1 case wide column.
  **/
  public var fruitsStuckInColumns(default, null): Bool = false;

  /**
  Fix spawn point of portal fields in the bottom row.
  **/
  public var respawnOfLateralVorticesOnLowestLine(default, null): Bool = false;

  /**
  Prevent bads from jumping if there is an obstacle on the way.
  **/
  public var fruitsCanJumpIntoAPlatform(default, null): Bool = false;

  /**
  Fix movement of wall walker bads over the top of the level.
  **/
  public var crawlingBadsOnHighestLine(default, null): Bool = false;

  /**
  Displays the "new land" annotation even at the start of supersets.
  **/
  public var displayNewLandOnFirstLevel(default, null): Bool = false;

  public static var DEFAULT(default, null): BugfixConfig = new BugfixConfig();

  private function new(): Void {}

  @:diFactory
  public static function fromHml(dataMod: Data): BugfixConfig {
    if (!dataMod.has(Obfu.raw("BugFix"))) {
      return BugfixConfig.DEFAULT;
    }

    return BugfixConfig.fromRaw(dataMod.get(Obfu.raw("BugFix")));
  }

  public static function fromRaw(data: Dynamic): BugfixConfig {
    if (!Reflect.isObject(data)) {
      throw new Error("TypeError: BugfixConfig should be an object");
    }

    var self = new BugfixConfig();

    self.canBeStuckOnTopOfLateralVortices = BugfixConfig.getBoolOr(data, Obfu.raw("canBeStuckOnTopOfLateralVortices"), self.canBeStuckOnTopOfLateralVortices);
    self.startAgainMusicIfSameID = BugfixConfig.getBoolOr(data, Obfu.raw("startAgainMusicIfSameID"), self.startAgainMusicIfSameID);
    self.lifeSharingInMulti = BugfixConfig.getBoolOr(data, Obfu.raw("lifeSharingInMulti"), self.lifeSharingInMulti);
    self.reducedDarknessInMulti = BugfixConfig.getBoolOr(data, Obfu.raw("reducedDarknessInMulti"), self.reducedDarknessInMulti);
    self.consecutiveScoreLevels = BugfixConfig.getBoolOr(data, Obfu.raw("consecutiveScoreLevels"), self.consecutiveScoreLevels);
    self.lycheesBreakOrder = BugfixConfig.getBoolOr(data, Obfu.raw("lycheesBreakOrder"), self.lycheesBreakOrder);
    self.progressiveLagInLongRuns = BugfixConfig.getBoolOr(data, Obfu.raw("progressiveLagInLongRuns"), self.progressiveLagInLongRuns);
    self.strawberriesCanMissEachOther = BugfixConfig.getBoolOr(data, Obfu.raw("strawberriesCanMissEachOther"), self.strawberriesCanMissEachOther);
    self.invisiblePortalsWhenReenteringLevel = BugfixConfig.getBoolOr(data, Obfu.raw("invisiblePortalsWhenReenteringLevel"), self.invisiblePortalsWhenReenteringLevel);
    self.loopingAnimationsWhenChangingDisguisesCanSoftlock = BugfixConfig.getBoolOr(data, Obfu.raw("loopingAnimationsWhenChangingDisguisesCanSoftlock"), self.loopingAnimationsWhenChangingDisguisesCanSoftlock);
    self.loopingAnimationsWhenChangingDisguises = BugfixConfig.getBoolOr(data, Obfu.raw("loopingAnimationsWhenChangingDisguises"), self.loopingAnimationsWhenChangingDisguises);
    self.intangibleWhileWarpingEIBombs = BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingEIBombs"), self.intangibleWhileWarpingEIBombs);
    self.intangibleWhileWarpingRedBombs = BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingRedBombs"), self.intangibleWhileWarpingRedBombs);
    self.intangibleWhileWarpingHits = BugfixConfig.getBoolOr(data, Obfu.raw("intangibleWhileWarpingHits"), self.intangibleWhileWarpingHits);
    self.playerPositionAfterWarp = BugfixConfig.getBoolOr(data, Obfu.raw("playerPositionAfterWarp"), self.playerPositionAfterWarp);
    self.keepPocketGuuRain = BugfixConfig.getBoolOr(data, Obfu.raw("keepPocketGuuRain"), self.keepPocketGuuRain);
    self.raysAtRespawn = BugfixConfig.getBoolOr(data, Obfu.raw("raysAtRespawn"), self.raysAtRespawn);
    self.mapScreenIndicators = BugfixConfig.getBoolOr(data, Obfu.raw("mapScreenIndicators"), self.mapScreenIndicators);
    self.negativeScoreDisplay = BugfixConfig.getBoolOr(data, Obfu.raw("negativeScoreDisplay"), self.negativeScoreDisplay);
    self.raysOnHighestLine = BugfixConfig.getBoolOr(data, Obfu.raw("raysOnHighestLine"), self.raysOnHighestLine);
    self.teleportersOnHighestLine = BugfixConfig.getBoolOr(data, Obfu.raw("teleportersOnHighestLine"), self.teleportersOnHighestLine);
    self.fruitsStuckInColumns = BugfixConfig.getBoolOr(data, Obfu.raw("fruitsStuckInColumns"), self.fruitsStuckInColumns);
    self.respawnOfLateralVorticesOnLowestLine = BugfixConfig.getBoolOr(data, Obfu.raw("respawnOfLateralVorticesOnLowestLine"), self.respawnOfLateralVorticesOnLowestLine);
    self.fruitsCanJumpIntoAPlatform = BugfixConfig.getBoolOr(data, Obfu.raw("fruitsCanJumpIntoAPlatform"), self.fruitsCanJumpIntoAPlatform);
    self.crawlingBadsOnHighestLine = BugfixConfig.getBoolOr(data, Obfu.raw("crawlingBadsOnHighestLine"), self.crawlingBadsOnHighestLine);
    self.displayNewLandOnFirstLevel = BugfixConfig.getBoolOr(data, Obfu.raw("displayNewLandOnFirstLevel"), self.displayNewLandOnFirstLevel);
    
    return self;
  }

  private static function getBoolOr(raw: Dynamic, key: String, defaultVal: Bool): Bool {
    if (!Reflect.hasField(raw, key)) {
      return defaultVal;
    }
    var val: Dynamic = Reflect.field(raw, key);
    if (!Std.is(val, Bool)) {
      throw new Error("TypeError: Invalid BugfixConfig field: " + key);
    }
    return val;
  }
}
