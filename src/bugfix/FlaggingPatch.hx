package bugfix;

import etwin.ds.WeakMap;
import etwin.ds.WeakSet;
import hf.Hf;
import patchman.IPatch;
import patchman.Ref;

class FlaggingPatch implements IPatch {
  private var elems: Null<WeakSet<Hf>>;
  private var flags: Null<WeakMap<Hf, Int>>;
  private var bits: Null<Int>;
  private var inner: IPatch;

  private function new(inner: IPatch) {
    this.inner = inner;
  }

  public static inline function flag(flags: WeakMap<Hf, Int>, addBits: Int, inner: IPatch) {
    var self = new FlaggingPatch(inner);
    self.flags = flags;
    self.bits = addBits;
    return self;
  }

  public static inline function set(set: WeakSet<Hf>, inner: IPatch) {
    var self = new FlaggingPatch(inner);
    self.elems = set;
    return self;
  }

  public function patch(hf: Hf): Bool {
    if (this.flags != null) {
      this.flags.set(hf, this.flags.get(hf) | this.bits);
    }
    if (this.elems != null) {
      this.elems.add(hf);
    }
    return this.inner.patch(hf);
  }
}
