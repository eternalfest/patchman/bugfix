package bugfix;

import etwin.ds.WeakMap;
import etwin.ds.WeakSet;
import etwin.ds.Nil;
import hf.entity.bad.walker.Fraise;
import hf.entity.bad.walker.Litchi;
import hf.entity.bad.walker.LitchiWeak;
import hf.entity.bad.Walker;
import hf.entity.bad.WallWalker;
import hf.entity.Player;
import hf.entity.shoot.Ball;
import hf.entity.Trigger;
import hf.Entity;
import hf.Hf;
import hf.levels.GameMechanics;
import hf.levels.SetManager;
import hf.levels.ScriptEngine;
import hf.mode.GameMode;
import hf.Mode;
import hf.FxManager;
import patchman.Assert;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Bugfix {
  public static var FIELDS_AT_SPAWN(default, never): IPatch = new PatchList([
    Ref.auto(Player.resurrect).after(function(hf: Hf, self: Player): Void {
      self.infix();
    }),
    Ref.auto(GameMechanics.onViewReady).after(function(hf: Hf, self: GameMechanics): Void {
      // The external bugfix uses `ViewManager` even if `game` is only defined in `GameMechanics`
      // TODO: Check if this difference is relevant
      if (self.game != null) {
        for (p in self.game.getPlayerList()) {
          p.infix();
        }
      }
    })
  ]);

  private static var TOP_ROW_FIELDS_FLAGS(default, never): WeakMap<Hf, Int> = new WeakMap();
  private static var TOP_ROW_FIELDS_COMMON(default, never): IPatch =
  Ref.auto(SetManager.getCase).prefix(function(hf: Hf, self: SetManager, pt: hf.Pos<Int>): Nil<Int> {
    if (self.current != null && pt.y == 0) {
      var caseType: Int = self.current.__dollar__map[pt.x][0];
      if (caseType < 0) {
        var flag = caseType == hf.Data.FIELD_TELEPORT ? 0x2 : 0x1;
        if ((TOP_ROW_FIELDS_FLAGS.get(hf) & flag) != 0)
          return Nil.some(caseType);
      }
    }
    return Nil.none();
  });

  public static var TOP_ROW_SIMPLE_FIELDS(default, never): IPatch = FlaggingPatch.flag(TOP_ROW_FIELDS_FLAGS, 0x1, TOP_ROW_FIELDS_COMMON);
  public static var TOP_ROW_TELEPORT_FIELDS(default, never): IPatch = FlaggingPatch.flag(TOP_ROW_FIELDS_FLAGS, 0x2, TOP_ROW_FIELDS_COMMON);

  private static var BAD_STUCK_IN_COLUMN_STATE(default, never): WeakMap<Walker, {moved: Float, oldCx: Float, oldCy: Float}> = new WeakMap();
  public static var BAD_STUCK_IN_COLUMN(default, never): IPatch = new PatchList([
    Ref.auto(Walker.update).before(function(hf: Hf, self: Walker): Void {
      var state = BAD_STUCK_IN_COLUMN_STATE.get(self);
      if (state == null) {
        state = {moved: 0, oldCx: self.cx, oldCy: self.cy};
      }

      if (self.isReady() && self.cx == state.oldCx && self.cy == state.oldCy) {
        var CASE_WIDTH: Float = hf.Data.CASE_WIDTH;
        if (state.moved > 2 * CASE_WIDTH) {
          self.infix();
        } else {
          state.moved += Math.abs(self.dx);
        }
      }
    }),
    Ref.auto(Walker.infix).before(function(hf: Hf, self: Walker): Void {
      // Reset state
      BAD_STUCK_IN_COLUMN_STATE.set(
        self,
        {moved: 0, oldCx: self.cx, oldCy: self.cy}
      );
    }),
  ]);

  public static var LYCHEE_ORDER(default, never): IPatch = new PatchList([
    Ref.auto(Litchi.weaken).after(function(hf: Hf, self: Litchi): Void {
      if (self.child == null) {
        return;
      }

      // TODO: Replace uniqId comparisons with object equality and `.indexOf`?
      var child: LitchiWeak = self.child;
      var uniqId = self.uniqId;
      var childUniqId = child.uniqId;

      // Swap `self` and `child` in the game entity lists.
      var typeBits: Int = hf.Data.type_bit;
      for (typeBit in (0...typeBits)) {
        var type: Int = 1 << typeBit;
        if (!(child.isType(type) && self.isType(type))) {
          continue;
        }
        var entities: Array<Entity> = self.game.lists[typeBit];
        if (entities.length == 0) {
          continue;
        }
        var lastIndex: Int = entities.length - 1;
        var last: Entity = entities[lastIndex];
        if (last.uniqId != childUniqId) {
          continue;
        }
        for (i in 0...entities.length) {
          var entity: Entity = entities[i];
          if (entity.uniqId == uniqId) {
            entities[i] = child;
            entities[lastIndex] = self;
            break;
          }
        }
      }

      // Swap the unique ids and depths
      child.uniqId = uniqId;
      self.uniqId = childUniqId;
      self.swapDepths(child);

      self.onLifeTimer();
    }),
    Ref.auto(Litchi.freeze).prefix(function(hf: Hf, self: Litchi, d: Float): Nil<Void> {
      // Fix for the "Paix intérieure" item
      if (self.child != null) {
        self.child.freeze(d);
        return Nil.some(null);
      } else {
        return Nil.none();
      }
    })
  ]);

  public static var MEMORY_LEAKS(default, never): IPatch = new PatchList([
    // Fix triggers not being cleared when an entity is destroyed
    Ref.auto(Trigger.destroy).before(function(hf: Hf, self: Trigger): Void {
      self.updateCoords();
      self.tRem(self.cx, self.cy);
    }),
    // Fix level sprite holders not being cleared on level exit
    Ref.auto(hf.levels.View.destroy).after(function(hf, self) {
      self._sprite_top.removeMovieClip();
      self._sprite_back.removeMovieClip();
    })
  ]);

  public static var STUCK_ON_PORTAL_FIELD(default, never): IPatch =
  Ref.auto(Player.onPortalRefusal).after(function(hf: Hf, self: Player): Void {
    self.dy = -3 * self.dy;
  });

  public static var MULTIPLE_EXTRA_LIFE_STEPS(default, never): IPatch =
  Ref.auto(Player.getScoreHidden).after(function(hf: Hf, self: Player, value: Int): Void {
    // The original code only checks the first life step; check the rest.
    var step: Null<Int> = hf.Data.EXTRA_LIFE_STEPS[self.extraLifeCurrent];
    while (step != null && self.score >= step) {
      self.lives += 1;
      self.game.gi.setLives(self.pid, self.lives);
      self.game.manager.logAction("$EL" + self.extraLifeCurrent);
      self.extraLifeCurrent += 1;
      step = hf.Data.EXTRA_LIFE_STEPS[self.extraLifeCurrent];
    }
  });

  public static var LIFESHARING(default, never): IPatch =
  Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
    // Steal the life before calling the original code (so it does not run its buggy lifesharing logic)
    if (self.lives == 0 && hf.GameManager.CONFIG.hasOption(hf.Data.OPT_LIFE_SHARING)) {
      for (p in self.game.getPlayerList()) {
        if (p.uniqId != self.uniqId && p.lives > 0) {
          p.lives -= 1;
          self.lives += 1;
          self.game.gi.setLives(p.pid, p.lives);
          self.game.gi.setLives(self.pid, self.lives);
          break;
        }
      }
    }
  });

  public static var REDUCE_DARKNESS_ONCE(default, never): IPatch =
  Ref.auto(GameMode.insertPlayer).after(function(hf: Hf, self: GameMode, cx: Float, cy: Float, res: Player): Void {
    // Prevent the quest 107 ("Et la lumière fût !") from reducing darkness
    // multiple times if there are multiple players.
    if (res.pid != 0) {
      res.fl_torch = false;
    }
  });

  public static var CONTINUE_MUSIC(default, never): IPatch =
  Ref.auto(Mode.playMusic).prefix(function(hf: Hf, self: Mode, id: Int): Nil<Void> {
    if (self.fl_music && self.currentTrack == id) {
      return Nil.some(null);
    }
    return Nil.none();
  });

  public static var IMPROVED_STRAWBERRY_AIM(default, never): IPatch =
  Ref.auto(Fraise.onShoot).after(function(hf: Hf, self: Fraise): Void {
    // We assume that `Data.BALL` is only set on `Ball` instances
    var balls: Array<Ball> = cast self.game.getList(hf.Data.BALL);
    for (ball in balls) {
      if (self.ballTarget != null
      && ball.targetCatcher == self.ballTarget
      && self.y == self.ballTarget.y
      && Math.abs(self.ballTarget.x - self.x) < 5) {
        // TODO: Check that the ball target is not the player?
        untyped { self.ballTarget.catchBall(ball); }
      }
    }
  });

  public static var INVISIBLE_PORTALS_AND_EXIT_CLEAR(default, never): IPatch = new PatchList([
    // Modify the injected script so that the t_pos node is deleted with we exit the level.
    // - If Merlin isn't used, the $exitClear implementation below is used;
    // - If Merlin >= v0.14.2 is used, the $exitClear implementation of Merlin is used, and the one below is a noop;
    // - If Merlin < v0.14.2 is used, this fix does nothing.
    Ref.auto(ScriptEngine.insertPortal).replace(function(hf: Hf, self: ScriptEngine, cx: Float, cy: Float, pid: Int): Void {
      self.addScript('<$$t_pos $$x="$cx" $$y="$cy" $$d="1" $$repeat="-1" $$exitClear="1"><$$e_portal $$pid="$pid"/></$$t_pos>');
    }),
    Ref.auto(GameMechanics.onReadComplete).before(removeExitClearScriptNodes),
    Ref.auto(GameMechanics.suspend).before(removeExitClearScriptNodes),
  ]);

  private static function removeExitClearScriptNodes(hf: Hf, self: GameMechanics): Void {
    if (self.scriptEngine == null) return;
    var script = self.scriptEngine.script;
    if (script == null) return;
    var cur = script.firstChild;
    while (cur != null) {
      var next = cur.nextSibling;
      // The ef typings aren't complete, so we need untyped to bypass them.
      if (untyped cur.get("$exitClear") == "1") {
        cur.removeNode();
      }
      cur = next;
    }
  }

  public static var JUMP_OBSTACLE(default, never): IPatch =
  Ref.auto(GameMechanics.parseCurrentIA).after(function(hf: Hf, self: GameMechanics, it: Dynamic): Void {
    // TODO: Only patch after the parsing is done
    var jumpLeft = hf.Data.IA_JUMP_LEFT, jumpRight = hf.Data.IA_JUMP_RIGHT, ground = hf.Data.GROUND;
    for (cy in 0...hf.Data.LEVEL_HEIGHT) {
      for (cx in 0...hf.Data.LEVEL_WIDTH) {
        var flag = self.flagMap[cx][cy];
        if (flag & jumpLeft > 0) {
          if (self.getCase({x: cx, y: cy - 1}) == ground || self.getCase({x: cx - 1, y: cy - 1}) == ground) {
            self.flagMap[cx][cy] = flag & ~jumpLeft;
          }
        }
        if (flag & jumpRight > 0) {
          if (self.getCase({x: cx, y: cy - 1}) == ground || self.getCase({x: cx + 1, y: cy - 1}) == ground) {
            self.flagMap[cx][cy] = flag & ~jumpRight;
          }
        }
      }
    }
  });

  public static var TOP_ROW_WALL_WALKER(default, never): IPatch =
  Ref.auto(WallWalker.wallWalkIA).replace(function(hf: Hf, self: WallWalker): Void {
    // Differences:
    // - Remove the `self.cy == 0` special-case
    // - Use a variant of `self.world.getCase` that treats all out-of-bounds as walls.
    // - Simplify the offset computation

    if (self.deathTimer > 0) {
      return;
    }
    var xoff: Int = self.dx < 0 ? -1 : self.dx > 0 ? 1 : 0;
    var yoff: Int = self.dy < 0 ? -1 : self.dy > 0 ? 1 : 0;
    var world = self.world;

    inline function isWall(x: Int, y: Int): Bool {
      return !world.inBound(x, y) || world.getCase({x: x, y: y}) > 0;
    }

    if (!isWall(self.cx + self.cp.x, self.cy + self.cp.y)) {
      self.setDir(self.cp.x, self.cp.y);
      self.setCP(-xoff, -yoff);
    } else if (isWall(self.cx + xoff, self.cy + yoff)) {
      self.setDir(-self.cp.x, -self.cp.y);
      self.setCP(xoff, yoff);
    }

    var isStuck: Bool = true;
    for (_ in 0...4) {
      xoff = self.dx < 0 ? -1 : self.dx > 0 ? 1 : 0;
      yoff = self.dy < 0 ? -1 : self.dy > 0 ? 1 : 0;
      if (isWall(self.cx + xoff, self.cy + yoff) && isWall(self.cx + self.cp.x, self.cy + self.cp.y)) {
        self.setDir(-self.cp.x, -self.cp.y);
        self.setCP(xoff, yoff);
      } else {
        isStuck = false;
        break;
      }
    }
    if (isStuck && self.deathTimer <= 0) {
      self.suicide();
    }
  });

  public static var NEW_LAND_START_SUPERSET(default, never): IPatch =
  Ref.auto(FxManager.attachLevelPop).wrap(function(h: Hf, self: FxManager, name: String, fl_label: Bool, old): Void {
    old(self, name, true);
  });

  private static var LOOPING_PLAYER_ALWAYS_FIX(default, never): WeakSet<Hf> = new WeakSet();
  public static var LOOPING_PLAYER_ANIMATIONS_SOFTLOCK(default, never): IPatch =
  Ref.auto(hf.entity.Player.replayAnim).wrap(function(hf, self, old) {
    var before = self.fl_loop;
    old(self);
    // Avoid looping when controls are locked to prevent softlock
    // but keep the historic and funny bug otherwise
    if (self.fl_lockControls || LOOPING_PLAYER_ALWAYS_FIX.exists(hf)) {
      self.fl_loop = before;
    }
  });

  public static var LOOPING_PLAYER_ANIMATIONS(default, never): IPatch =
    FlaggingPatch.set(LOOPING_PLAYER_ALWAYS_FIX, LOOPING_PLAYER_ANIMATIONS_SOFTLOCK);

  public static var MAP_SCREEN_INDICATORS(default, never): IPatch =
  Ref.auto(hf.mode.GameMode.usePortal).replace(function(hf: Hf, self: hf.mode.GameMode, pid: Int, e: hf.Entity): Bool {
    // Copied from original code, except for commented parts.
    if (self.nextLink != null) return false;

    var Data = hf.Data;
    var link = Data.getLink(self.currentDim, self.world.currentId, pid);
    if (link == null) return false;

    // `mapScreenIndicators` fix: don't increment the lid.
    var msg = self.world.currentId + ". " + hf.Lang.getLevelName(link.to_did, link.to_lid);
    var fl_right = self.flipCoordReal(e.x) >= Data.GAME_WIDTH * 0.5;
    self.fl_rightPortal = fl_right;
    self.registerMapEvent(fl_right ? Data.EVENT_EXIT_RIGHT : Data.EVENT_EXIT_LEFT, msg);

    if (e == null) {
      var inf = Math.POSITIVE_INFINITY;
      for (p in self.getPlayerList()) {
        p.dx = (self.portalMcList[pid].x - p.x) * 0.018;
        p.dy = (self.portalMcList[pid].y - p.y) * 0.018;
        p.fl_hitWall = false;
        p.fl_hitGround = false;
        p.fl_gravity = false;
        p.fl_friction = false;
        p.specialMan.clearTemp();
        p.unshield();
        p.lockControls(inf);
        p.playAnim(Data.ANIM_PLAYER_DIE);
      }
      self.nextLink = link;
      return true;
    }
    self.switchDimensionById(link.to_did, link.to_lid, link.to_pid);
    return true;
  });

  public static var PLAYER_POS_AFTER_WARP(default, never): IPatch =
  Ref.auto(hf.entity.Player.infix).replace(function(hf, self) {
    Ref.call(hf, super.infix, self);

    // Don't execute the rest of Player.infix if we switched levels,
    // because our position wasn't yet updated.
    if (self.world.fl_restoring)
      return;

    // Copied from the original code.
    var v3 = self.world.getCase({x: self.cx, y: self.cy});
    if (v3 > hf.Data.FIELD_TELEPORT && v3 < 0) {
      if (self.currentWeapon != -v3) {
        var v4 = self.game.fxMan.attachShine(self.x, self.y - hf.Data.CASE_HEIGHT * 0.5);
        v4.mc._xscale = 65;
        v4.mc._yscale = v4.mc._xscale;
        self.game.soundMan.playSound("sound_field", hf.Data.CHAN_FIELD);
      }
      self.changeWeapon(-v3);
    }
    if (v3 == hf.Data.FIELD_PEACE) {
      if (self.currentWeapon != -v3) {
        var v5 = self.game.fxMan.attachShine(self.x, self.y - hf.Data.CASE_HEIGHT * 0.5);
        v5.mc._xscale = 65;
        v5.mc._yscale = v5.mc._xscale;
        self.game.soundMan.playSound("sound_field", hf.Data.CHAN_FIELD);
      }
      self.changeWeapon(hf.Data.WEAPON_NONE);
    }
    if (!self.fl_kill && !self.fl_destroy) {
      self.game.world.scriptEngine.onEnterCase(self.cx, self.cy);
    }
    self.showTeleporters();
  });

  public static var POCKET_GUU_RAIN(default, never): IPatch =
  Ref.auto(FxManager.inGameParticlesDir).wrap(function(hf: Hf, self: FxManager, id: Int, x: Float, y: Float, n: Int, dir: Null<Float>, old): Void {
    var isPocketGuuActive = false;
    for (p in self.game.getPlayerList()) {
      if (p.specialMan.actives[100]) {
        isPocketGuuActive = true;
        break;
      }
    }
    var config = hf.GameManager.CONFIG;
    var tempEnable = false;
    if (isPocketGuuActive && !config.fl_detail && id == hf.Data.PARTICLE_RAIN) {
      tempEnable = true;
      config.fl_detail = true;
      hf.Data.MAX_FX *= 2;
    }
    old(self, id, x, y, n, dir);
    if (tempEnable) {
      config.fl_detail = false;
      hf.Data.MAX_FX = Math.ceil(hf.Data.MAX_FX * 0.5);
    }
  });

  public static var INTANGIBLE_WHILE_WARPING_EI_BOMBS(default, never): IPatch =
    Ref.auto(hf.entity.bomb.PlayerBomb.onExplode).wrap(function(hf, self, old): Void {
      if (self.game.fl_bombExpert && self.game.nextLink != null) {
        self.game.fl_bombExpert = false;
        old(self);
        self.game.fl_bombExpert = true;
      }
      else
        old(self);
    });
  public static var INTANGIBLE_WHILE_WARPING_RED_BOMBS(default, never): IPatch =
    Ref.auto(hf.entity.bomb.player.Red.onExplode).prefix(function(hf, self) {
      return self.game.nextLink == null ? Nil.none() : Nil.some(null);
    });
  public static var INTANGIBLE_WHILE_WARPING_HITS(default, never): IPatch =
    Ref.auto(hf.entity.Player.killHit).prefix(function(hf, self, dx) {
      return self.game.nextLink == null ? Nil.none() : Nil.some(null);
    });

  public static var NEGATIVE_SCORE_DISPLAY(default, never): IPatch = new PatchList([
    // This doesn't add separators on negative numbers, fix it.
    Ref.auto(hf.Data.formatNumber).wrap(function(hf, n, old) {
      var txt = old(n < 0 ? -n : n);
      return n < 0 ? '-$txt' : txt;
    }),
    // This may add an extra space after the '-', fix it too.
    Ref.auto(hf.gui.GameInterface.getScoreTxt).wrap(function(hf, self, n, old) {
      var txt = old(self, n < 0 ? -n : n);
      return n < 0 ? '-$txt' : txt;
    }),
    // Add proper support to negative score deltas to fake score logic.
    Ref.auto(hf.gui.GameInterface.update).replace(function(hf, self) {
      if (!self.fl_print) {
        for (i in 0...self.scores.length) {
          if (self.scores[i] != null) {
            var fakeScore: Int = self.fakeScores[i];
            var realScore: Int = self.realScores[i];
            if (fakeScore < realScore) {
              fakeScore += Math.round(Math.max(90, (realScore - fakeScore) / 5));
              if (fakeScore > realScore)
                fakeScore = realScore;
            }
            if (fakeScore > realScore) {
              fakeScore += Math.round(Math.min(-90, (realScore - fakeScore) / 5));
              if (fakeScore < realScore)
                fakeScore = realScore;
            }
            self.fakeScores[i] = fakeScore;

            self.scores[i].text = self.getScoreTxt(fakeScore);
          }
        }
      }
    }),
  ]);

  @:diExport
  public var patch(default, null): IPatch;

  public function new(config: BugfixConfig): Void {
    var patches: Array<IPatch> = [];

    inline function addIf(add: Bool, patch: IPatch) {
      if (add)
        patches.push(patch);
    }

    addIf(config.raysAtRespawn, FIELDS_AT_SPAWN);
    addIf(config.raysOnHighestLine, TOP_ROW_SIMPLE_FIELDS);
    addIf(config.teleportersOnHighestLine, TOP_ROW_TELEPORT_FIELDS);
    addIf(config.fruitsStuckInColumns, BAD_STUCK_IN_COLUMN);
    addIf(config.lycheesBreakOrder, LYCHEE_ORDER);
    addIf(config.progressiveLagInLongRuns, MEMORY_LEAKS);
    addIf(config.canBeStuckOnTopOfLateralVortices, STUCK_ON_PORTAL_FIELD);
    addIf(config.consecutiveScoreLevels, MULTIPLE_EXTRA_LIFE_STEPS);
    addIf(config.lifeSharingInMulti, LIFESHARING);
    addIf(config.reducedDarknessInMulti, REDUCE_DARKNESS_ONCE);
    addIf(config.startAgainMusicIfSameID, CONTINUE_MUSIC);
    if (config.respawnOfLateralVorticesOnLowestLine) {
      Assert.debug("NotImplemented: respawnOfLateralVorticesOnLowestLine".length == 0);
    }
    addIf(config.strawberriesCanMissEachOther, IMPROVED_STRAWBERRY_AIM);
    addIf(config.fruitsCanJumpIntoAPlatform, JUMP_OBSTACLE);
    addIf(config.crawlingBadsOnHighestLine, TOP_ROW_WALL_WALKER);
    addIf(config.invisiblePortalsWhenReenteringLevel, INVISIBLE_PORTALS_AND_EXIT_CLEAR);
    addIf(config.displayNewLandOnFirstLevel, NEW_LAND_START_SUPERSET);
    addIf(config.loopingAnimationsWhenChangingDisguisesCanSoftlock, LOOPING_PLAYER_ANIMATIONS_SOFTLOCK);
    addIf(config.loopingAnimationsWhenChangingDisguises, LOOPING_PLAYER_ANIMATIONS);
    addIf(config.playerPositionAfterWarp, PLAYER_POS_AFTER_WARP);
    addIf(config.keepPocketGuuRain, POCKET_GUU_RAIN);
    addIf(config.mapScreenIndicators, MAP_SCREEN_INDICATORS);
    addIf(config.negativeScoreDisplay, NEGATIVE_SCORE_DISPLAY);
    addIf(config.intangibleWhileWarpingEIBombs, INTANGIBLE_WHILE_WARPING_EI_BOMBS);
    addIf(config.intangibleWhileWarpingRedBombs, INTANGIBLE_WHILE_WARPING_RED_BOMBS);
    addIf(config.intangibleWhileWarpingHits, INTANGIBLE_WHILE_WARPING_HITS);

    this.patch = new PatchList(patches);
  }
}
