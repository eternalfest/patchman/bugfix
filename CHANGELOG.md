# 0.11.7 (2024-01-08)
- **[Fix]** Fix a compilation error introduced by moulins being incapable of refactoring a single line of code without fucking it up.

# 0.11.6 (2024-01-08)
- **[Fix]** Fix an issue with the `intangibleWhileWarping` fixes.

# 0.11.5 (2024-01-06)
- **[Feature]** Add `negativeScoreDisplay` fix, enabled by default.
- **[Internal]** Set `"preferUnplugged": true` for Yarn PnP.

# 0.11.4 (2024-01-04)
- **[Fix]** Properly swap the litchis in entity lists on `lycheesBreakOrder`, making `Tournelune`, `Jugement dernier` and `Jugement avant-dernier` work on weakened (in-order) litchis.

# 0.11.3 (2023-12-05)
- **[Feature]** Add set of 3 fixes related to entities interacting with the player during the portal warping animation: `intangibleWhileWarpingEIBombs`, `intangibleWhileWarpingRedBombs`, and `intangibleWhileWarpingHits`.
- **[Breaking change]** Remove `canDieWhileWarping` (replaced with `intangibleWhileWarpingHits`).

# 0.11.2 (2023-11-18)
- **[Feature]** Add `loopingAnimationsWhenChangingDisguisesCanSoftlock` fix.
    - _Note:_ `loopingAnimationsWhenChangingDisguises` fix is now disabled by default.

# 0.11.1 (2023-09-22)
- **[Fix]** Allow external code to create custom `BugfixConfig`s.

# 0.11.0 (2023-02-03)
- **[Feature]** `canDieWhileWarping` now protects players on the first frame of a warp.
- **[Feature]** Add `mapScreenIndicators` fix.

# 0.10.0 (2021-04-20)

- **[Breaking change]** Update to `patchman@0.10.3`.
- **[Feature]** Add `keepPocketGuuRain` fix.
- **[Internal]** Update to Yarn 2.

# 0.9.3 (2021-02-20)

- **[Feature]** Add `playerPositionAfterWarp` fix.
- **[Feature]** Add `canDieWhileWarping` fix.
- **[Feature]** Add `loopingAnimationsWhenChangingDisguises` fix.
- **[Feature]** `progressiveLagInLongRuns` gained a fix for a MovieClip leak in `hf.level.View`.

# 0.9.2 (2021-01-25)

- **[Feature]** Add `displayNewLandOnFirstLevel` fix.

# 0.9.1 (2020-12-04)

- **[Feature]** Add `invisiblePortalsWhenReenteringLevel` fix.
  - _Warning:_ This fix does not work with Merlin < v0.14.2.
- **[Fix]** Use `prefix` instead of `wrap` for improved performance.

# 0.9.0 (2020-09-02)

- **[Breaking change]** Update to `patchman@0.9.0`.

# 0.8.1 (2020-02-18)

- **[Fix]** Read BugFix configuration from patcher data.

# 0.8.0 (2020-01-08)

- **[Breaking change]** Update to `patchman@0.8.0`.

# 0.4.0 (2020-01-07)

- **[Breaking change]** Update to `patchman@0.7.2`.

# 0.3.0 (2019-11-02)

- **[Breaking change]** Update to `patchman@0.6.2`.

# 0.2.0 (2019-09-19)

- **[Breaking change]** Update to `patchman@0.5`.

# 0.1.0 (2019-09-17)

- **[Feature]** First release.
